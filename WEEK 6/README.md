# Lecture 6: Spring Boot introduction + Spring REST MVC + MyBatis CRUD

## Prerequisites
0. Install [Postman](https://www.getpostman.com)
1. Install [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/download/download-thanks.html?platform=windows&code=IIC)
    1. Install IntelliJ Lombok plugin
    2. Install Database navigator plugin
2. Install [PostgresSQL](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads) version 10.5
    - Download PostgresSql from the link provided and execute .exe file
    - Click Next.
     
    ![Setup Guide](images/postgresql/install1.png)
    
    - Click Next.
     
    ![Setup Guide](images/postgresql/install2.png)
    
    - Click Next.
     
    ![Setup Guide](images/postgresql/install3.png)
    
    - Click Next.
     
    ![Setup Guide](images/postgresql/install4.png)
    
    - Provide password **root** for superuser.
     
    ![Setup Guide](images/postgresql/install5.png)
    
    - Click Next.
     
    ![Setup Guide](images/postgresql/install6.png)
    
    - Select **English, Unated States** locale.
     
    ![Setup Guide](images/postgresql/install7.png)
    
    - Click Next.
     
    ![Setup Guide](images/postgresql/install8.png)
    
    - Click Calcel to exit Stack Builder wizard.
     
    ![Setup Guide](images/postgresql/install9.png)
    
3. Create **feedback** database in the DB server.
    - Launch pgAdmin
     
    ![Setup Guide](images/postgresql/install10.png)
    
    - Connect server using **root** password
     
    ![Setup Guide](images/postgresql/install11.png)
    
    - Click menu Object → Create → Database...
      
    ![Setup Guide](images/postgresql/install12.png)
    
    - Provide database name **feedback** and click **Save**
     
    ![Setup Guide](images/postgresql/install13.png)
    

## Links
* [Get Started with Postman](https://learning.getpostman.com/getting-started/)
* [Project Lombok](https://projectlombok.org)
* [Jackson Project (JSON parser for Java)](https://github.com/FasterXML/jackson)
* [Jackson Annotations](https://github.com/FasterXML/jackson-annotations/wiki/Jackson-Annotations)
