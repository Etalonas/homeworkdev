package com.devbridge.sourcery.feedback.service;

import com.devbridge.sourcery.feedback.api.dto.QuestionDTO;
import com.devbridge.sourcery.feedback.exception.ObjectNotFoundException;
import com.devbridge.sourcery.feedback.mapper.AnswerMapper;
import com.devbridge.sourcery.feedback.mapper.QuestionMapper;
import com.devbridge.sourcery.feedback.model.Answer;
import com.devbridge.sourcery.feedback.model.Question;
import com.devbridge.sourcery.feedback.model.QuestionType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class QuestionServiceTest {
    @Mock
    private AnswerMapper answerMapper;
    @Mock
    private QuestionMapper questionMapper;
    @InjectMocks
    private QuestionService questionService;


    @Test(expected = ObjectNotFoundException.class)
    public void throwExceptionWhenQuestionNotFound() {
        questionService.getQuestionById(1);
        verify(questionMapper, times(1)).findQuestionById(1);
        verify(answerMapper, times(0)).findAllByQuestionId(any());
    }

    @Test
    public void successfullyGetQuestion() {
        when(questionMapper.findQuestionById(1)).thenReturn(Question.builder().id(1).build());
        when(answerMapper.findAllByQuestionId(1)).thenReturn(Collections.singletonList(Answer.builder().build()));
        Question question = questionService.getQuestionById(1);
        assertFalse(question.getAnswers().isEmpty());
    }

    @Test
    public void successfullyCreateQuestion() {
        when(questionMapper.findQuestionById(1)).thenReturn(new Question());

        QuestionDTO questionDTO = QuestionDTO.builder()
                .id(1)
                .text("text")
                .quizId(1)
                .type("RADIO")
                .build();
        questionService.createQuestion(questionDTO);
        verify(questionMapper, times(1)).createQuestion(
                questionDTO.getId(), questionDTO.getText(), QuestionType.RADIO, questionDTO.getQuizId()
        );
        verify(questionMapper, times(1)).findQuestionById(1);
    }

    @Test(expected = RuntimeException.class)
    public void deleteQuestionThatDoesNotExist() {
        questionService.deleteQuestion(1);
    }

    @Test
    public void deleteQuestionSuccessfully() {
        when(questionMapper.deletebyId(1)).thenReturn(true);
        questionService.deleteQuestion(1);
    }
}
