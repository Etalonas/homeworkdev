package com.devbridge.sourcery.feedback.api;

import com.devbridge.sourcery.feedback.service.QuestionService;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(QuestionController.class)
public class QuestionControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    QuestionService questionService;

    @Test
    public void getQuestionByCodeShouldCallServiceMethodFindById() throws Exception {
        mockMvc.perform(get("/api/question/1"))
                .andExpect(status().isOk());
        verify(questionService, times(1)).getQuestionById(1);
    }
    @Test
    public void createQuestionSuccessfullyAPI() throws Exception {
        mockMvc.perform(post("/api/question")
                .contentType(MediaType.APPLICATION_JSON).content(new JSONObject().toString()))
                .andExpect(status().isOk());
        verify(questionService, times(1)).createQuestion(any());
    }
    @Test
    public void deleteQuestionSuccessfullyAPI() throws Exception {
        mockMvc.perform(delete("/api/question/1"))
                .andExpect(status().isOk());
        verify(questionService, times(1)).deleteQuestion(1);
    }

}
