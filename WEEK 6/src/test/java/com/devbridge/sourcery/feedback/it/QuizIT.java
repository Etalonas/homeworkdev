package com.devbridge.sourcery.feedback.it;

import com.devbridge.sourcery.feedback.api.QuizController;
import com.devbridge.sourcery.feedback.api.dto.QuizDTO;
import com.devbridge.sourcery.feedback.mapper.QuizMapper;
import com.devbridge.sourcery.feedback.model.Quiz;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Objects;

@SpringBootTest
@RunWith(SpringRunner.class)
public class QuizIT {
    @Autowired
    QuizController quizController;
    @Autowired
    QuizMapper quizMapper;

    Logger logger = LoggerFactory.getLogger(SpringRunner.class);

    @Test
    public void createQuizSuccessfullyAndDeleteByTitle() {
        QuizDTO quizDTO = new QuizDTO(5,"code", "testTitle", "desc", true);
        Quiz quiz = quizController.createQuiz(quizDTO).getBody();
        logger.debug("{}", quiz);
        Assert.assertTrue(Objects.equals(quiz.getCode(), quizDTO.getCode()));
        Assert.assertTrue(Objects.equals(quiz.getDescription(), quizDTO.getDescription()));
        Assert.assertTrue(Objects.equals(quiz.getTitle(), quizDTO.getTitle()));
        Assert.assertTrue(Objects.equals(quiz.isActive(), quizDTO.isActive()));
        Assert.assertTrue(quiz.getId() != null);
        quizMapper.deleteQuizByTitle(quiz.getTitle());
    }
}
