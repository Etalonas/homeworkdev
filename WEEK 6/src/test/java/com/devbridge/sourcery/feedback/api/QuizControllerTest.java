package com.devbridge.sourcery.feedback.api;

import com.devbridge.sourcery.feedback.service.QuizService;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(QuizController.class)
public class QuizControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    QuizService quizService;

    @Test
    public void getQuizByCodeShouldCallServiceMethodFindByCode() throws Exception {
        mockMvc.perform(get("/api/quiz/anyId"))
                .andExpect(status().isOk());
        verify(quizService, times(1)).findByCode("anyId");
    }

    @Test
    public void createQuizSuccessfullyAPI() throws Exception {
        mockMvc.perform(post("/api/quiz")
                .contentType(MediaType.APPLICATION_JSON).content(new JSONObject().toString()))
                .andExpect(status().isOk());
        verify(quizService, times(1)).createQuiz(any());
    }
    @Test
    public void deleteQuizSuccessfullyAPI() throws Exception {
        mockMvc.perform(delete("/api/quiz/1"))
                .andExpect(status().isOk());
        verify(quizService, times(1)).deleteQuiz(1);
    }
}
