package com.devbridge.sourcery.feedback.it;

import com.devbridge.sourcery.feedback.api.QuestionController;
import com.devbridge.sourcery.feedback.api.dto.QuestionDTO;
import com.devbridge.sourcery.feedback.mapper.QuestionMapper;
import com.devbridge.sourcery.feedback.model.Question;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class QuestionIT {
    @Autowired
    QuestionController questionController;
    @Autowired
    QuestionMapper questionMapper;

    Logger logger = LoggerFactory.getLogger(QuestionIT.class);


    @Test
    public void successfullyGetQuestion() {
        ResponseEntity<Question> responseEntity = questionController.getQuestionById(1); // Find from database
        Assert.assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);
        logger.debug("{}", responseEntity.getBody());
        Assert.assertFalse(responseEntity.getBody().getAnswers().isEmpty());
    }

    @Test
    public void successfullyCreateAndDeleteQuestion() {
        QuestionDTO questionDTO = QuestionDTO.builder()
                .id(999)
                .text("testing")
                .type("RADIO")
                .quizId(1)
                .build();
        Question question = questionController.createQuestion(questionDTO).getBody(); // Find from database
        logger.debug("{}", question);
        Assert.assertTrue(question != null);
        Assert.assertTrue(questionMapper.deletebyId(questionDTO.getId()));
    }
}
