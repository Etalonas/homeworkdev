package com.devbridge.sourcery.feedback.it;

import com.devbridge.sourcery.feedback.api.AnswerController;
import com.devbridge.sourcery.feedback.api.dto.AnswerDTO;
import com.devbridge.sourcery.feedback.mapper.AnswerMapper;
import com.devbridge.sourcery.feedback.model.Answer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.AssertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AnswerIT {
    @Autowired
    AnswerController answerController;
    @Autowired
    AnswerMapper answerMapper;

    @Test
    public void successfullySaveAnswer() {
        AnswerDTO answerDTO = new AnswerDTO(999, "test Answer", 1);
        Answer answer = answerController.createAnswer(answerDTO).getBody();
        Assert.assertTrue(answer != null);
        Assert.assertTrue(answerMapper.deleteById(answer.getId()));
    }
}
