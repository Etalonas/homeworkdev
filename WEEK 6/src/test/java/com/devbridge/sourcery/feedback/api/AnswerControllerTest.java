package com.devbridge.sourcery.feedback.api;

import com.devbridge.sourcery.feedback.service.AnswerService;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AnswerController.class)
public class AnswerControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    AnswerService answerService;

    @Test
    public void getAnswerByIdShouldCallServiceMethodFindById() throws Exception {
        mockMvc.perform(get("/api/answer/1"))
                .andExpect(status().isOk());
        verify(answerService, times(1)).getAnswerById(1);
    }

    @Test
    public void createAnswerSuccessfullyAPI() throws Exception {
        mockMvc.perform(post("/api/answer")
                .contentType(MediaType.APPLICATION_JSON).content(new JSONObject().toString()))
                .andExpect(status().isOk());
        verify(answerService, times(1)).createAnswer(any());
    }

    @Test
    public void deleteAnswerSuccessfullyAPI() throws Exception {
        mockMvc.perform(delete("/api/answer/1"))
                .andExpect(status().isOk());
        verify(answerService, times(1)).deleteAnswer(1);
    }

}
