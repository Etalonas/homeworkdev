package com.devbridge.sourcery.feedback.service;

import com.devbridge.sourcery.feedback.api.dto.QuizDTO;
import com.devbridge.sourcery.feedback.exception.ObjectNotFoundException;
import com.devbridge.sourcery.feedback.mapper.AnswerMapper;
import com.devbridge.sourcery.feedback.mapper.QuestionMapper;
import com.devbridge.sourcery.feedback.mapper.QuizMapper;
import com.devbridge.sourcery.feedback.model.Answer;
import com.devbridge.sourcery.feedback.model.Question;
import com.devbridge.sourcery.feedback.model.Quiz;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class QuizServiceTest {
    @Mock
    private QuizMapper quizMapper;
    @Mock
    private QuestionMapper questionMapper;
    @Mock
    private AnswerMapper answerMapper;
    @InjectMocks
    private QuizService quizService;

    @Test(expected = ObjectNotFoundException.class)
    public void expectNotFoundExceptionWhenBadCode() {
        quizService.findByCode("bad");
        verify(quizMapper, times(1)).findByCode("bad");

    }

    @Test
    public void properlyCollectsInformation() {
        List<Question> questionList = singletonList(Question.builder().id(1).build());
        List<Answer> answerList = singletonList(Answer.builder().id(1).build());
        Quiz quiz = Quiz.builder().id(1).build();
        when(quizMapper.findByCode("goodCode")).thenReturn(quiz);
        when(questionMapper.findAllByQuizId(quiz.getId())).thenReturn(questionList);
        when(answerMapper.findAllByQuestionId(1)).thenReturn(answerList);
        quiz = quizService.findByCode("goodCode");
        assertFalse(quiz.getQuestions().isEmpty());
        verify(quizMapper, times(1)).findByCode("goodCode");
        verify(questionMapper, times(1)).findAllByQuizId(1);
        verify(answerMapper, times(1)).findAllByQuestionId(1);
    }

    @Test
    public void createQuizSuccessfully() {
        when(quizMapper.findByCode(any())).thenReturn(new Quiz());
        QuizDTO quizDTO = new QuizDTO(2, "code", "title", "descr", false);
        quizService.createQuiz(quizDTO);
        //TODO implement auto id generator???
        verify(quizMapper, times(1)).createQuiz(
                quizDTO.getId(), quizDTO.getCode(), quizDTO.getTitle(), quizDTO.getDescription(), quizDTO.isActive()
        );
    }

    @Test(expected = RuntimeException.class)
    public void deleteQuizThatDoesNotExist() {
        quizService.deleteQuiz(1);
    }

    @Test
    public void deleteQuizSuccessfully() {
        when(quizMapper.deleteQuiz(1)).thenReturn(true);
        quizService.deleteQuiz(1);
    }
}
