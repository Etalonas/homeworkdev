package com.devbridge.sourcery.feedback.service;

import com.devbridge.sourcery.feedback.api.dto.AnswerDTO;
import com.devbridge.sourcery.feedback.exception.ObjectNotFoundException;
import com.devbridge.sourcery.feedback.mapper.AnswerMapper;
import com.devbridge.sourcery.feedback.model.Answer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AnswerServiceTest {
    @InjectMocks
    AnswerService answerService;
    @Mock
    AnswerMapper answerMapper;

    @Test(expected = ObjectNotFoundException.class)
    public void throwExceptionWhenAnswerNotFound() {
        answerService.getAnswerById(1);
        verify(answerMapper, times(1)).findById(1);
    }

    @Test
    public void findAnswerSuccessfully() {
        Answer answer = Answer.builder().build();
        when(answerMapper.findById(1)).thenReturn(answer);
        Assert.assertEquals(answer, answerService.getAnswerById(1));
    }

    @Test
    public void successfullyCreateAnswer() {
        when(answerMapper.findById(1)).thenReturn(new Answer());

        AnswerDTO answerDTO = AnswerDTO.builder()
                .id(1)
                .text("text")
                .questionId(1)
                .build();
        answerService.createAnswer(answerDTO);
        verify(answerMapper, times(1)).createAnswer(
                answerDTO.getId(), answerDTO.getText(), answerDTO.getQuestionId()
        );
        verify(answerMapper, times(1)).findById(1);
    }

    @Test(expected = RuntimeException.class)
    public void deleteAnswerThatDoesNotExist() {
        answerService.deleteAnswer(1);
    }

    @Test
    public void deleteAnswerSuccessfully() {
        when(answerMapper.deleteById(1)).thenReturn(true);
        answerService.deleteAnswer(1);
    }
}
