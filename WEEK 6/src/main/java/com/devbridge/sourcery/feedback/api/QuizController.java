package com.devbridge.sourcery.feedback.api;

import com.devbridge.sourcery.feedback.api.dto.QuizDTO;
import com.devbridge.sourcery.feedback.model.Quiz;
import com.devbridge.sourcery.feedback.service.QuizService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/quiz")
public class QuizController {

    private final QuizService quizService;
    Logger logger = LoggerFactory.getLogger(QuizController.class);


    public QuizController(QuizService quizService) {
        this.quizService = quizService;
    }

    @PostMapping
    public ResponseEntity<Quiz> createQuiz(@RequestBody QuizDTO quizDTO) {
        Quiz createdQuiz = quizService.createQuiz(quizDTO);
        return new ResponseEntity<>(createdQuiz, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Quiz> updateQuiz(@RequestBody Quiz quiz) {
        logger.debug("updateQuiz: {}", quiz);

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{quizCode}")
    public ResponseEntity<Quiz> getQuizByCode(@PathVariable String quizCode) {
        return new ResponseEntity<>(quizService.findByCode(quizCode), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteQuizByCode(@PathVariable Integer id) {
        quizService.deleteQuiz(id);
    }

}
