package com.devbridge.sourcery.feedback.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuestionDTO {

    private Integer id;

    private String text;

    private String type;

    private Integer quizId;

}
