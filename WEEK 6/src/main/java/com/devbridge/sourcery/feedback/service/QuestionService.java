package com.devbridge.sourcery.feedback.service;

import com.devbridge.sourcery.feedback.api.dto.QuestionDTO;
import com.devbridge.sourcery.feedback.exception.ObjectNotFoundException;
import com.devbridge.sourcery.feedback.mapper.AnswerMapper;
import com.devbridge.sourcery.feedback.mapper.QuestionMapper;
import com.devbridge.sourcery.feedback.model.Question;
import com.devbridge.sourcery.feedback.model.QuestionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.devbridge.sourcery.feedback.message.Error.DELETE_FAILED;
import static com.devbridge.sourcery.feedback.message.Error.QUESTION_NOT_FOUND;

@Service
public class QuestionService {
    private final QuestionMapper questionMapper;
    private final AnswerMapper answerMapper;

    @Autowired
    public QuestionService(QuestionMapper questionMapper, AnswerMapper answerMapper) {
        this.questionMapper = questionMapper;
        this.answerMapper = answerMapper;
    }

    @Transactional
    public List<Question> getAllQuestions(Integer quizId) {
        return questionMapper.findAllByQuizId(quizId);
    }

    @Transactional
    public Question getQuestionById(Integer questionId) {
        Question question = questionMapper.findQuestionById(questionId);
        if (question == null) {
            throw new ObjectNotFoundException(QUESTION_NOT_FOUND);
        }
        question.setAnswers(answerMapper.findAllByQuestionId(question.getId()));
        return question;
    }

    public Question createQuestion(QuestionDTO questionDTO) {
        questionMapper.createQuestion(
                questionDTO.getId(),
                questionDTO.getText(),
                QuestionType.valueOf(questionDTO.getType()),
                questionDTO.getQuizId());
        return getQuestionById(questionDTO.getId());

    }
    @Transactional
    public void deleteQuestion(Integer id) {
        Boolean deleted = questionMapper.deletebyId(id);
        if (!deleted) {
            throw new RuntimeException(DELETE_FAILED);
        }
    }
}
