package com.devbridge.sourcery.feedback.service;

import com.devbridge.sourcery.feedback.api.dto.QuizDTO;
import com.devbridge.sourcery.feedback.exception.ObjectNotFoundException;
import com.devbridge.sourcery.feedback.mapper.AnswerMapper;
import com.devbridge.sourcery.feedback.mapper.QuestionMapper;
import com.devbridge.sourcery.feedback.mapper.QuizMapper;
import com.devbridge.sourcery.feedback.model.Question;
import com.devbridge.sourcery.feedback.model.Quiz;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.devbridge.sourcery.feedback.message.Error.DELETE_FAILED;
import static com.devbridge.sourcery.feedback.message.Error.QUIZ_NOT_FOUND;

@Service
public class QuizService {

    private final QuizMapper quizMapper;
    private final QuestionMapper questionMapper;
    private final AnswerMapper answerMapper;


    public QuizService(QuizMapper quizMapper, QuestionMapper questionMapper, AnswerMapper answerMapper) {
        this.quizMapper = quizMapper;
        this.questionMapper = questionMapper;
        this.answerMapper = answerMapper;
    }

    public Quiz findByCode(String code) {
        Quiz quiz = quizMapper.findByCode(code);
        if (quiz == null) throw new ObjectNotFoundException(QUIZ_NOT_FOUND);
        List<Question> questionList = questionMapper.findAllByQuizId(quiz.getId());
        questionList.forEach(question -> question.setAnswers(answerMapper.findAllByQuestionId(question.getId())));
        quiz.setQuestions(questionList);
        return quiz;
    }

    @Transactional
    public Quiz createQuiz(QuizDTO quizDTO) {
        quizMapper.createQuiz(
                quizDTO.getId(), quizDTO.getCode(), quizDTO.getTitle(), quizDTO.getDescription(), quizDTO.isActive()
        );
        return findByCode(quizDTO.getCode());
    }

    @Transactional
    public void deleteQuiz(Integer quizId) {
        Boolean deleted = quizMapper.deleteQuiz(quizId);
        if (!deleted) {
            throw new RuntimeException(DELETE_FAILED);
        }
    }


}
