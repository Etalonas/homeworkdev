package com.devbridge.sourcery.feedback;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.devbridge.sourcery.feedback.mapper")
public class PersistenceConfig {
}
