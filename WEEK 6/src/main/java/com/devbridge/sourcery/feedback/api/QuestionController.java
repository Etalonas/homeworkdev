package com.devbridge.sourcery.feedback.api;

import com.devbridge.sourcery.feedback.api.dto.QuestionDTO;
import com.devbridge.sourcery.feedback.model.Question;
import com.devbridge.sourcery.feedback.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/question")
public class QuestionController {
    private final QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Question> getQuestionById(@PathVariable Integer id) {
        return new ResponseEntity<>(questionService.getQuestionById(id), HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Question> createQuestion(@RequestBody QuestionDTO questionDTO){
        return new ResponseEntity<>(questionService.createQuestion(questionDTO),HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public void deleteQuestion(@PathVariable Integer id){
        questionService.deleteQuestion(id);
    }

}
