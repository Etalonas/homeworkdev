package com.devbridge.sourcery.feedback.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Quiz {

    private Integer id;

    private String code;

    private String title;

    private String description;

    private boolean active;

    private List<Question> questions;

}
