package com.devbridge.sourcery.feedback.message;

public class Error {
    public static final String QUIZ_NOT_FOUND = "Quiz was not found";
    public static final String QUESTION_NOT_FOUND = "Question was not found";
    public static final String ANSWER_NOT_FOUND = "Answer was not found";
    public static final String DELETE_FAILED = "Something went wrong";

}
