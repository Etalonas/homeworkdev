package com.devbridge.sourcery.feedback.mapper;

import com.devbridge.sourcery.feedback.model.Answer;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AnswerMapper {
    @Select("Select * from answer where question_id = #{questionId}")
    List<Answer> findAllByQuestionId(@Param("questionId") Integer questionId);

    @Select("Select * from answer where id = #{id}")
    Answer findById(@Param("id") Integer id);

    @Insert("insert into answer(id,text,question_id) values(#{id},#{text},#{question_id})")
    void createAnswer(@Param("id") Integer id,
                      @Param("text") String text,
                      @Param("question_id") Integer questionId);

    @Update("DELETE FROM answer WHERE id=#{id}")
    boolean deleteById(@Param("id") Integer id);

}
