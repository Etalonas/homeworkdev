package com.devbridge.sourcery.feedback.service;

import com.devbridge.sourcery.feedback.api.dto.AnswerDTO;
import com.devbridge.sourcery.feedback.exception.ObjectNotFoundException;
import com.devbridge.sourcery.feedback.mapper.AnswerMapper;
import com.devbridge.sourcery.feedback.model.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.devbridge.sourcery.feedback.message.Error.ANSWER_NOT_FOUND;
import static com.devbridge.sourcery.feedback.message.Error.DELETE_FAILED;

@Service
public class AnswerService {
    private final AnswerMapper answerMapper;

    @Autowired
    public AnswerService(AnswerMapper answerMapper) {
        this.answerMapper = answerMapper;
    }

    @Transactional
    public Answer getAnswerById(Integer id) {
        Answer answer = answerMapper.findById(id);
        if (answer == null) {
            throw new ObjectNotFoundException(ANSWER_NOT_FOUND);
        }
        return answer;
    }

    public Answer createAnswer(AnswerDTO answerDTO) {
        answerMapper.createAnswer(answerDTO.getId(), answerDTO.getText(), answerDTO.getQuestionId());
        return getAnswerById(answerDTO.getId());
    }

    public void deleteAnswer(Integer id) {
        Boolean deleted = answerMapper.deleteById(id);
        if (!deleted) {
            throw new RuntimeException(DELETE_FAILED);
        }
    }
}
