package com.devbridge.sourcery.feedback.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuizDTO {
    private Integer id;

    private String code;

    private String title;

    private String description;

    private boolean active;


}
