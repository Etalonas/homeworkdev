package com.devbridge.sourcery.feedback.mapper;

import com.devbridge.sourcery.feedback.model.Question;
import com.devbridge.sourcery.feedback.model.QuestionType;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface QuestionMapper {
    @Select("Select * from question where quiz_id = #{quizId}")
    List<Question> findAllByQuizId(@Param("quizId") Integer quizId);

    @Select("Select * from question where id = #{id}")
    Question findQuestionById(@Param("id") Integer id);

    @Insert("insert into question(id,text,type,quiz_id) values(#{id},#{text},#{type},#{quiz_id})")
    void createQuestion(@Param("id") Integer id,
                        @Param("text") String text,
                        @Param("type") QuestionType type,
                        @Param("quiz_id") Integer quizId);

    @Update("DELETE FROM question WHERE id=#{id}")
    boolean deletebyId(@Param("id") Integer id);
}
