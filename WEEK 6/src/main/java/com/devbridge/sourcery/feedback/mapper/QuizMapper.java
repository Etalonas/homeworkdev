package com.devbridge.sourcery.feedback.mapper;


import com.devbridge.sourcery.feedback.model.Quiz;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;


@Repository
@Mapper
public interface QuizMapper {

    @Select("SELECT * FROM quiz WHERE code = #{code}")
    Quiz findByCode(@Param("code") String code);

    @Update("DELETE FROM quiz WHERE id=#{quizId}")
    boolean deleteQuiz(@Param("quizId") Integer quizId);

    @Update("DELETE FROM quiz WHERE title=#{title}")
    void deleteQuizByTitle(@Param("title") String title);

    @Insert("insert into quiz(id,code,title,description,active) values(#{id},#{code},#{title},#{description},#{active})")
    void createQuiz(@Param("id") Integer id,
                    @Param("code") String code,
                    @Param("title") String title,
                    @Param("description") String description,
                    @Param("active") boolean active);

}
