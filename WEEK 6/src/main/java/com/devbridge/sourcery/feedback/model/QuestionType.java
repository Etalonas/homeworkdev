package com.devbridge.sourcery.feedback.model;

public enum QuestionType {

    SHORT, LONG, CHECK, RADIO

}
