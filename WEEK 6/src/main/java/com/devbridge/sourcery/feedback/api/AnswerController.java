package com.devbridge.sourcery.feedback.api;

import com.devbridge.sourcery.feedback.api.dto.AnswerDTO;
import com.devbridge.sourcery.feedback.model.Answer;
import com.devbridge.sourcery.feedback.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/answer")
public class AnswerController {
    private final AnswerService answerService;

    @Autowired
    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Answer> getAnswerById(@PathVariable Integer id) {
        return new ResponseEntity<>(answerService.getAnswerById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Answer> createAnswer(@RequestBody AnswerDTO answerDTO) {
        return new ResponseEntity<>(answerService.createAnswer(answerDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteAnswer(@PathVariable Integer id) {
        answerService.deleteAnswer(id);
    }
}
