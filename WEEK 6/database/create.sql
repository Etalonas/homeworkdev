CREATE TABLE quiz
(
    "id" bigserial NOT NULL,
    "code" character varying(64) NOT NULL,
    "title" character varying(256) NOT NULL,
    "description" character varying(2048),
    "active" boolean NOT NULL default false,
    PRIMARY KEY ("id")
);

CREATE TABLE question
(
    "id" bigserial NOT NULL ,
    "text" character varying(1024) NOT NULL,
    "type" character varying(64) NOT NULL,
    "quiz_id" bigint NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "QUESTION_QUIZ_FK" FOREIGN KEY ("quiz_id")
        REFERENCES quiz ("id") MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE TABLE answer
(
    "id" bigserial NOT NULL ,
    "text" character varying(1024) NOT NULL,
    "question_id" bigint NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "ANSWER_QUESTION_FK" FOREIGN KEY ("question_id")
    REFERENCES question ("id") MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE
);
