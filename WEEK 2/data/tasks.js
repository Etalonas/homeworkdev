const tasks = [
    {
        title: 'Walk the dog',
        completed: false
    },
    {
        title: 'Take out the trash',
        completed: true
    },
    {
        title: 'Vacuum the carpet',
        completed: true
    },
    {
        title: 'Make dinner',
        completed: false
    }
];

export default tasks;