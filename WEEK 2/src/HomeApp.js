import MarkedNumbers from './MarkedNumbers';

class HomeApp {
    constructor() {
        this.markedNumbers = new MarkedNumbers();
        this.renderTasks();
    }

    renderTasks() {
        let list = document.querySelector('.js-add-task-list');
        list.innerHTML = '';

        this.markedNumbers.startNumbers.forEach((number) => {
            let newTask = document.createElement('div');
            newTask.innerText = number;

            let mark = document.createElement('button');
            mark.addEventListener('click', () => {
                this.markedNumbers.markUnmarkNumbers(number);
                this.renderTasks();
            });
            mark.className = this.markedNumbers.checkedNumbers.has(number) ? 'glyphicon glyphicon-remove' : 'glyphicon glyphicon-screenshot';
            newTask.appendChild(mark);
            list.appendChild(newTask);
        });
        let submitButton = document.createElement('button');
        submitButton.addEventListener('click', () => {
            this.markedNumbers.deleteData();
            this.renderTasks();
        });
        submitButton.innerText = 'Submit';
        list.appendChild(submitButton);
    }
}

export default HomeApp;