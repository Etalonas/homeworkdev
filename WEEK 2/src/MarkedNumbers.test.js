const MarkerNumbers = require('./MarkedNumbers');

test('Numbers starts with 5 numbers', () => {
    expect(new MarkerNumbers().startNumbers.size).toBe(5);
});

test('At the beginning there are no marked numbers', () => {
    expect(new MarkerNumbers().checkedNumbers.size).toBe(0);
});

test('Numbers are from 1 to 5', () => {
    const markedNumbers = new MarkerNumbers();
    new Set([1, 2, 3, 4, 5]).forEach((number) => {
        expect(markedNumbers.startNumbers.has(number)).toBe(true);

    })
});
test('You can mark number successfully', () => {
    const markedNumbers = new MarkerNumbers();
    markedNumbers.markUnmarkNumbers(1);
    expect(markedNumbers.checkedNumbers.size).toBe(1);
    expect(markedNumbers.checkedNumbers.has(1)).toBe(true);
});
test('You can unmark number successfully', () => {
    const markedNumbers = new MarkerNumbers();
    markedNumbers.markUnmarkNumbers(1);
    markedNumbers.markUnmarkNumbers(1);
    expect(markedNumbers.checkedNumbers.size).toBe(0);
});
test('You can remove numbers successfully', () => {
    const markedNumbers = new MarkerNumbers();
    markedNumbers.markUnmarkNumbers(1);
    markedNumbers.markUnmarkNumbers(2);
    markedNumbers.deleteData();
    expect(markedNumbers.startNumbers.size).toBe(3);
    expect(markedNumbers.startNumbers.has(1)).toBe(false);
    expect(markedNumbers.startNumbers.has(2)).toBe(false);

});
