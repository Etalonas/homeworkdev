class MarkedNumbers {
    constructor() {
        this.startNumbers = new Set([1, 2, 3, 4, 5]);
        this.checkedNumbers = new Set();
    }

    markUnmarkNumbers(number) {
        if (!this.checkedNumbers.delete(number)) {
            this.checkedNumbers.add(number);
        }
    }

    deleteData() {
        this.checkedNumbers.forEach(
            (number) => this.startNumbers.delete(number)
        );
    }
}

module.exports = MarkedNumbers;