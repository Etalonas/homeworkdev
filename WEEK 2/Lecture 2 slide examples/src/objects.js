
let obj = {}                // Object literal
obj = Object.create(Foo);   // Creates object and assign Foo as prototype
obj = new Foo('text');      // Using function constructor 

function Foo(a) { 
    this.a = a;             // no implicit "this" usage
}

Foo.prototype.sayHi = function() {  // obj.__proto__ = Foo.prototype
    console.log('hi');
}

