
function a() {
    const param = 'param reached by closure'; 
    
    return function b() {
        return param;
    }
}

console.log(a()()); // prints param


