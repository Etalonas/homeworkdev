
let a = 1;  // Number type
a = 'foo';  // String type
a = true;   // Boolean

a = [];     // Object type
a = {};     // Object type

a = undefined; // undefined type

a = function () {   // Function type
}

console.log(typeof a);  // Return function type


