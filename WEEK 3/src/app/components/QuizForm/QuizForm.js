import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button/Button';

import Header from 'components/Header/Header';
import Question from 'components/Question/Question';

import './quizForm.scss';
import Typography from '@material-ui/core/Typography/Typography';
import ResultMessageComponent from 'components/ResultMessage/ResultMessageComponent';

class QuizForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formValid: true,
            showAnswers: false,
            correctAnswers: 0,
        };
    }

    /* eslint-disable react/sort-comp */
    feedback;
    /* eslint-enable */

    componentDidMount = () => {
        this.feedback = {
            quizId: this.props.quiz.id,
            answers: new Map(),
        };
    };

    handleChange = (id, value) => {
        if (value.length === 0) {
            this.feedback.answers.delete(id);
        } else {
            this.feedback.answers.set(id, value);
        }
    };

    isFormValid = () => {
        const { quiz } = this.props;
        const { answers } = this.feedback;
        return quiz.questions.length === answers.size
            && this.multipleAnswersValidation(
                quiz.questions.filter(question => question.multipleAnswers),
                answers,
            );
    }

    multipleAnswersValidation=(questions, answers) => questions.every(
        question => question.correctAnswer.length === answers.get(question.id).length,
    )

    handleSubmit = (e) => {
        e.preventDefault();
        const isValid = this.isFormValid();
        if (isValid) {
            this.checkCorrectAnswers();
        }
        this.setState({ formValid: isValid, showAnswers: isValid });
    };

    checkCorrectAnswers = () => {
        let correctAnswers = 0;
        this.props.quiz.questions.forEach((question) => {
            // ALL ANSWERS ARE CORRECT
            if (!question.correctAnswer) {
                correctAnswers += 1;
            } else if (!question.multipleAnswers) {
                if (this.singleAnswerCheck(question)) {
                    correctAnswers += 1;
                }
            } else if (question.multipleAnswers) {
                if (this.multipleAnswerCheck(question)) {
                    correctAnswers += 1;
                }
            }
        });
        this.setState({
            correctAnswers,
            showAnswers: true,
        });
    }

    singleAnswerCheck = question => this.feedback.answers.get(question.id).toString()
        === question.correctAnswer.toString()

    multipleAnswerCheck = question => question.correctAnswer.every(
        answer => this.feedback.answers.get(question.id).includes(answer),
    )

    render() {
        const { isLoading, error, quiz } = this.props;

        return (
            <form className="quiz-form" noValidate onSubmit={this.handleSubmit}>
                <Header text={quiz.title} />
                {error && (
                    <Typography className="description" variant="h5" color="inherit">
                        {error}
                    </Typography>
                )}
                {this.state.showAnswers && (
                    <ResultMessageComponent correctAnswers={this.state.correctAnswers} />
                )}
                {!this.state.formValid && (
                    <Typography className="description" variant="h5" color="error">
                        Not all questions have been answered
                    </Typography>
                )}
                {quiz.questions && quiz.questions.map(q => (
                    <Question
                        key={q.id}
                        data={q}
                        onChange={value => this.handleChange(q.id, value)}
                    />
                ))}
                <div className="control-section">
                    <Button
                        disabled={isLoading || !(quiz.id)}
                        variant="contained"
                        color="primary"
                        onClick={this.handleSubmit}
                        type="submit"
                    >
                        {isLoading ? 'Saving...' : 'Submit'}
                    </Button>
                    <Button variant="contained" type="button">
                        Cancel
                    </Button>
                </div>
            </form>
        );
    }
}

QuizForm.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    quiz: PropTypes.shape({
        id: PropTypes.string,
        code: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        active: PropTypes.bool,
        questions: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number,
            text: PropTypes.string,
            type: PropTypes.string,
            answers: PropTypes.arrayOf(PropTypes.shape({
                id: PropTypes.number,
                text: PropTypes.string,
            })),
        })),
    }).isRequired,
};

export default QuizForm;
