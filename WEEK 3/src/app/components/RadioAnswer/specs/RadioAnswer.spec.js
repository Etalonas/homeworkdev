import React from 'react';
import { shallow } from 'enzyme';
import quiz from 'data/quiz';
import RadioAnswer from '../RadioAnswer';

describe('RadioAnswer', () => {
    it('should have only one value', () => {
        const functionCalled = jest.fn();
        const radioAnswer = shallow(
            <RadioAnswer
                answers={quiz.questions.find(q => q.id === 1).answers}
                onChange={functionCalled}
            />,
        );

        const radioGroup = radioAnswer.find('.radio-group');
        radioGroup.simulate('change', { target: { value: '1' } });
        expect(radioAnswer.state().value).toEqual('1');
        expect(functionCalled).toHaveBeenCalledTimes(1);

        radioGroup.simulate('change', { target: { value: '2' } });
        expect(radioAnswer.state().value).toEqual('2');
        expect(functionCalled).toHaveBeenCalledTimes(2);
    });
});
