import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField/TextField';

class TextAnswer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: '',
        };
    }

    handleChange = (e) => {
        const { value } = e.target;

        this.setState({
            value,
        });

        this.props.onChange(value);
    };

    render() {
        return (
            <TextField
                label="Your Answer"
                margin="normal"
                fullWidth
                multiline={this.props.multiLine}
                value={this.state.value}
                onChange={this.handleChange}
                type="text"
            />
        );
    }
}

TextAnswer.propTypes = {
    multiLine: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
};

TextAnswer.defaultProps = {
    multiLine: false,
};

export default TextAnswer;
