import React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextAnswer from 'components/TextAnswer/TextAnswer';
import PropTypes from 'prop-types';

const ResultMessageComponent = props => (
    <div>
        <Paper elevation={1}>
            <Typography component="p" align="center">
                You scored
                {' '}
                {props.correctAnswers}
            </Typography>
        </Paper>
    </div>
);
export default ResultMessageComponent;

TextAnswer.propTypes = {
    correctAnswers: PropTypes.number,
};
TextAnswer.defaultProps = {
    correctAnswers: 0,
};
