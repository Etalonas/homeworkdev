const quiz = {
    id: 'some-id-123',
    code: 'VLN_3',
    title: 'Sample quiz',
    description: 'i have no idea what i am doing',
    active: true,
    questions: [
        {
            id: 1,
            text: 'How are you?',
            type: 'RADIO',
            correctAnswer: 5,
            answers: [
                {
                    id: 1,
                    text: 'yes',
                },
                {
                    id: 2,
                    text: 'left',
                },
                {
                    id: 3,
                    text: 'red',
                },
                {
                    id: 4,
                    text: '12',
                },
                {
                    id: 5,
                    text: 'fine',
                },
            ],
        },
        {
            id: 2,
            text: 'Why?',
            type: 'LONG',
        },
        {
            id: 3,
            text: 'Favorite animal?',
            type: 'SHORT',
        },
        {
            id: 4,
            text: 'Which options you prefer',
            type: 'CHECK',
            multipleAnswers: true,
            correctAnswer: ['Correct 1', 'Correct 2'],
            answers: [
                {
                    id: 1,
                    text: 'Option 1',
                },
                {
                    id: 2,
                    text: 'Option 2',
                },
                {
                    id: 3,
                    text: 'Option 3',
                },
                {
                    id: 4,
                    text: 'Correct 1',
                },
                {
                    id: 5,
                    text: 'Correct 2',
                },
            ],
        },
    ],
};

export default quiz;
