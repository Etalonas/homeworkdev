import React from 'react';
import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog/Dialog';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import Typography from '@material-ui/core/Typography/Typography';

const TextDialog = ({ handleClose, open, text }) => (
    <Dialog
        open={open}
        onClose={handleClose}
        scroll="paper"
        aria-labelledby="scroll-dialog-title"
    >
        <div className="dialog-title">
            <Typography variant="h6" color="inherit">
                Quiz JSON
            </Typography>
        </div>
        <DialogContent>
            <div>
                <pre>{text}</pre>
            </div>
        </DialogContent>
    </Dialog>
);

TextDialog.propTypes = {
    handleClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired,
};

export default TextDialog;
