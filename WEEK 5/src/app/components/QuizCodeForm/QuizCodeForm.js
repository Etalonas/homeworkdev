import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField/TextField';
import Button from '@material-ui/core/Button/Button';
import Typography from '@material-ui/core/Typography/Typography';
import { Link } from 'react-router-dom';

import './quizCodeForm.scss';

class QuizCodeForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            code: '',
        };
    }

    handleChange = (e) => {
        const code = e.target.value.trim();

        if (code !== this.state.code) {
            this.setState({
                code,
            });
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.submitForm(this.state.code);
    };

    isDisabled() {
        const { isLoading } = this.props;
        const { code } = this.state;

        return code.length <= 3 || isLoading;
    }

    render() {
        const { error, activeCode } = this.props;

        return (
            <form className="quiz-code-form" noValidate onSubmit={this.handleSubmit}>
                <div className="quiz-code">
                    <TextField
                        error={!!error}
                        label={error || 'Quiz Code'}
                        margin="normal"
                        onChange={this.handleChange}
                        required
                    />
                </div>
                <Button
                    disabled={this.isDisabled()}
                    variant="contained"
                    color="primary"
                    size="large"
                    type="submit"
                >
                    Start
                </Button>
                <Link to="/generator">
                    <Button
                        variant="contained"
                        color="secondary"
                        size="large"
                    >
                        New
                    </Button>
                </Link>
                <Link to="/results">
                    <Button
                        variant="contained"
                        color="secondary"
                        size="large"
                    >
                        Quiz-test results
                    </Button>
                </Link>
                <div>
                    <Typography className="description" variant="h5" color="inherit">
                        {`Active quiz: ${activeCode}`}
                    </Typography>
                    <Link to="/quiz">
                        Go to active quiz
                    </Link>
                </div>
            </form>
        );
    }
}

QuizCodeForm.defaultProps = {
    activeCode: '',
};

QuizCodeForm.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    submitForm: PropTypes.func.isRequired,
    error: PropTypes.string.isRequired,
    activeCode: PropTypes.string,
};

export default QuizCodeForm;
