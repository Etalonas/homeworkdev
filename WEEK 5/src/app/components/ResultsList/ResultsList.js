import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

const renderResultListItem = result => (
    <ListItem key={result.submitTime}>
        <ListItemText primary={result.quizCode} />
        <ListItemText primary={result.correctAnswers} />
        <ListItemText primary={result.totalAnswers} />
        <ListItemText primary={result.submitTime} />
    </ListItem>
);

const ResultsList = ({ resultsList, views }) => (
    <React.Fragment>
        <List>
            <ListItem>
                <ListItemText primary="Quiz" />
                <ListItemText primary="Correct answers" />
                <ListItemText primary="Total answers" />
                <ListItemText primary="Submit time" />
            </ListItem>
            <Divider />
            {resultsList.map(renderResultListItem)}
            <Divider />
            <ListItem>
                <ListItemText primary={`Views in current session: ${views}`} />
            </ListItem>
        </List>
    </React.Fragment>
);

ResultsList.propTypes = {
    resultsList: PropTypes.arrayOf(PropTypes.shape({
        quizId: PropTypes.string.isRequired,
        quizCode: PropTypes.string.isRequired,
        correctAnswers: PropTypes.number.isRequired,
        totalAnswers: PropTypes.number.isRequired,
        submitTime: PropTypes.string.isRequired,
    })).isRequired,
    views: PropTypes.number.isRequired,
};

export default ResultsList;
