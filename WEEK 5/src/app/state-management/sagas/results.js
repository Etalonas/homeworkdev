import {
    all,
    put,
    takeLatest,
    takeEvery,
} from 'redux-saga/effects';

import { SUBMIT_RESULTS, GET_RESULTS, CLEAR_RESULTS } from 'state-management/constants/results';
import {
    submitResultsSuccess,
    submitResultsError,
    getResultsSuccess,
    getResultsError,
} from 'state-management/actions/results';
import Storage from 'utils/storage';

const RESULT_STORAGE = 'results';

function* submitResults({ results }) {
    try {
        const oldResults = yield Storage.load(RESULT_STORAGE, []);
        const newResults = [
            ...oldResults,
            results,
        ];
        yield Storage.save(RESULT_STORAGE, newResults);
        yield put(submitResultsSuccess(results));
    } catch (e) {
        const error = (e && e.error) ? e.error : 'System error';
        yield put(submitResultsError(error));
    }
}

function* clearResults() {
    try {
        yield Storage.save(RESULT_STORAGE, []);
    } catch (e) {
        const error = (e && e.error) ? e.error : 'System error';
        yield put(submitResultsError(error));
    }
}

function* getResults() {
    try {
        const results = yield Storage.load(RESULT_STORAGE, []);
        yield put(getResultsSuccess(results));
    } catch (e) {
        const error = (e && e.error) ? e.error : 'System error';
        yield put(getResultsError(error));
    }
}

function* resultsSaga() {
    yield all([
        takeLatest(SUBMIT_RESULTS, submitResults),
        takeLatest(CLEAR_RESULTS, clearResults),
        takeEvery(GET_RESULTS, getResults),
    ]);
}

export default resultsSaga;
