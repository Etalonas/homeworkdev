import { all, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';

import { GET_QUIZ, QUIZ_VIEW_URL } from 'state-management/constants/quiz';
import { getQuizSuccess } from 'state-management/actions/quiz';
import Api from 'utils/api';
import Storage from 'utils/storage';


const QUIZ_STORAGE = 'quiz';
function* getQuiz({ code }) {
    try {
        const quiz = yield Api.get(QUIZ_VIEW_URL(code));
        console.log(quiz);
        yield put(getQuizSuccess(quiz));
        yield put(push('/quiz'));
    } catch (e) {
        const quiz = yield Storage.load(QUIZ_STORAGE);
        yield put(getQuizSuccess(quiz));
        yield put(push('/quiz'));
    }
}

function* quizSaga() {
    yield all([
        takeLatest(GET_QUIZ, getQuiz),
    ]);
}

export default quizSaga;
