import { all, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';

import {
    POST_FEEDBACK,
    POST_FEEDBACK_URL,
} from 'state-management/constants/feedback';
import {
    postFeedbackError,
    postFeedbackSuccess,
} from 'state-management/actions/feedback';
import Api from 'utils/api';

function* postFeedback({ feedback }) {
    try {
        yield Api.post(POST_FEEDBACK_URL, feedback);
        yield put(postFeedbackSuccess());
        yield put(push('/'));
    } catch (e) {
        const error = (e && e.error) ? e.error : 'System error';
        yield put(postFeedbackError(error));
    }
}

function* feedbackSaga() {
    yield all([
        takeLatest(POST_FEEDBACK, postFeedback),
    ]);
}

export default feedbackSaga;
