import {
    GET_QUIZ,
    GET_QUIZ_ERROR,
    GET_QUIZ_SUCCESS,
} from 'state-management/constants/quiz';

export const getQuiz = code => ({
    type: GET_QUIZ,
    code,
});

export const getQuizSuccess = quiz => ({
    type: GET_QUIZ_SUCCESS,
    quiz,
});

export const getQuizError = error => ({
    type: GET_QUIZ_ERROR,
    error,
});
