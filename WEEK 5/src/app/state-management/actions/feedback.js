import {
    POST_FEEDBACK,
    POST_FEEDBACK_SUCCESS,
    POST_FEEDBACK_ERROR,
} from 'state-management/constants/feedback';

export const postFeedback = feedback => ({
    type: POST_FEEDBACK,
    feedback,
});

export const postFeedbackSuccess = () => ({
    type: POST_FEEDBACK_SUCCESS,
});

export const postFeedbackError = error => ({
    type: POST_FEEDBACK_ERROR,
    error,
});
