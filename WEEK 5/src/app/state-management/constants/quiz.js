// export const QUIZ_VIEW_URL = code => `/api/quiz/${code}`;
export const QUIZ_VIEW_URL = code => `/src/data/quiz_${code}.json`;

export const GET_QUIZ = 'quiz/GET_QUIZ';
export const GET_QUIZ_SUCCESS = 'quiz/GET_QUIZ_SUCCESS';
export const GET_QUIZ_ERROR = 'quiz/GET_QUIZ_ERROR';
