import {
    SUBMIT_RESULTS,
    SUBMIT_RESULTS_SUCCESS,
    SUBMIT_RESULTS_ERROR,
    CLEAR_RESULTS,
    UPDATE_VIEWS,
    GET_RESULTS,
    GET_RESULTS_SUCCESS,
    GET_RESULTS_ERROR,
} from 'state-management/constants/results';

const initialState = {
    list: [],
    views: 0,
    error: null,
    isLoading: false,
    isLoadedOnce: false,
};

export default function resultsReducer(state = initialState, action = {}) {
    switch (action.type) {
    case GET_RESULTS:
    case SUBMIT_RESULTS: return {
        ...state,
        isLoading: true,
    };
    case SUBMIT_RESULTS_SUCCESS: return {
        ...state,
        list: [
            ...state.list,
            action.results,
        ],
        error: null,
        isLoading: false,
    };
    case SUBMIT_RESULTS_ERROR: return {
        ...state,
        error: action.error,
        isLoading: false,
    };
    case CLEAR_RESULTS: return {
        ...state,
        list: [],
    };
    case UPDATE_VIEWS: return {
        ...state,
        views: state.views + 1,
    };
    case GET_RESULTS_SUCCESS: return {
        ...state,
        list: action.results || initialState,
        error: null,
        isLoading: false,
        isLoadedOnce: true,
    };
    case GET_RESULTS_ERROR: return {
        ...state,
        error: action.error,
        isLoading: false,
    };
    default:
        return state;
    }
}
