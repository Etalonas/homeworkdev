import React from 'react';
import PropTypes from 'prop-types';
import connect from 'react-redux/es/connect/connect';

import { Header, QuizCodeForm } from 'components';
import { getQuiz as getQuizAction } from 'state-management/actions/quiz';

class Home extends React.PureComponent {
    render() {
        const {
            isLoading,
            error,
            getQuiz,
            quizCode,
        } = this.props;

        return (
            <React.Fragment>
                <Header title="Feedback Matters" />
                <QuizCodeForm
                    isLoading={isLoading}
                    error={error}
                    submitForm={getQuiz}
                    activeCode={quizCode}
                />
            </React.Fragment>
        );
    }
}

Home.propTypes = {
    isLoading: PropTypes.bool,
    error: PropTypes.string,
    getQuiz: PropTypes.func,
    quizCode: PropTypes.string,
};

Home.defaultProps = {
    isLoading: false,
    error: '',
    getQuiz: () => {},
    quizCode: '',
};

const mapStateToProps = state => ({
    isLoading: state.quiz.isLoading,
    error: state.quiz.error,
    quizCode: state.quiz.quiz.code,
});

const mapDispatchToProps = dispatch => ({
    getQuiz: code => dispatch(getQuizAction(code)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
