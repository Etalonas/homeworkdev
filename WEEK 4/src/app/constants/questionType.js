const questionType = {
    CHECK: 'CHECK',
    LONG: 'LONG',
    RADIO: 'RADIO',
    SHORT: 'SHORT',
};

export default questionType;
