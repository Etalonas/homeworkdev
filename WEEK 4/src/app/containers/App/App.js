import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import {
    QuizGenerator, Home, Quiz, SessionResult,
} from 'containers';

import './app.scss';

export default () => (
    <div className="app">
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/generator" component={QuizGenerator} />
            <Route path="/quiz" component={Quiz} />
            <Route path="/session/:quizid" component={SessionResult} />
            <Redirect to="/" />
        </Switch>
    </div>
);
