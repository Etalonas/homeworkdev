import React from 'react';
import connect from 'react-redux/es/connect/connect';
import PropTypes from 'prop-types';

import { Header, QuizViewForm, TextDialog } from 'components';
import QUESTION_TYPE from 'constants/questionType';
import {
    resetAnswers as resetAnswersAction,
    setAnswer as setAnswerAction,
} from 'state-management/actions/answers';
import {
    submitResults,
} from 'state-management/actions/results';

import Typography from '@material-ui/core/Typography/Typography';

class Quiz extends React.Component {
    constructor(props) {
        super(props);

        const answers = {};

        props.quiz.questions.forEach((q) => {
            answers[q.id] = q.type === QUESTION_TYPE.CHECK ? [] : '';
        });

        this.state = {
            feedback: {
                quizId: props.quiz.id,
                answers,
            },
            open: false,
            answered: false,
            correctAnswers: 0,
        };
    }

    componentDidMount() {
        this.props.resetAnswers();
    }

    checkAnswer = (question) => {
        const { answers } = this.state.feedback;
        switch (question.type) {
        case QUESTION_TYPE.CHECK:
            return this.isQuestionMultiAnswered(question)
                && (answers[question.id].length === question.correctAnswers.length);
        case QUESTION_TYPE.RADIO:
        case QUESTION_TYPE.SHORT:
        case QUESTION_TYPE.LONG:
        default:
            return this.isQuestionAnswered(question);
        }
    }

    isQuestionAnswered = (question) => {
        const { answers } = this.state.feedback;
        const { correctAnswers, id } = question;
        return correctAnswers.includes(answers[id].toUpperCase());
    }

    isQuestionMultiAnswered = (question) => {
        const { answers } = this.state.feedback;
        const { correctAnswers, id } = question;
        return answers[id].every(answer => correctAnswers.includes(answer.toUpperCase()));
    }

    checkAnswers = () => {
        const { quiz } = this.props;
        const quizWithCapitalizedAnswers = {
            ...quiz,
            questions: quiz.questions.map(question => ({
                ...question,
                correctAnswers: question.correctAnswers.map(a => a.toUpperCase()),
            })),
        };

        let correctAnswers = 0;
        for (let i = 0; i < quizWithCapitalizedAnswers.questions.length; i += 1) {
            if (this.checkAnswer(quizWithCapitalizedAnswers.questions[i])) {
                correctAnswers += 1;
            }
        }
        return correctAnswers;
    }

    handleChange = (id, value) => {
        const { feedback } = this.state;

        feedback.answers[id] = value;
        this.props.setAnswer(id, value);

        this.setState({
            feedback,
        });
    };

    handleClose = () => {
        this.setState({
            open: false,
        });
    };

    handleSubmit = () => {
        const correctAnswers = this.checkAnswers();
        const totalQuestions = this.props.quiz.questions.length;
        this.setState({ correctAnswers, answered: true });
        this.props.submitResults({
            quizId: this.props.quiz.code,
            correctAnswers,
            totalQuestions,
        });
    };

    render() {
        const { quiz } = this.props;
        return (
            <React.Fragment>
                <Header title={quiz.title} />
                <QuizViewForm
                    feedback={this.state.feedback}
                    handleChange={this.handleChange}
                    handleSubmit={this.handleSubmit}
                    isLoading={false}
                    error=""
                    quiz={quiz}
                />
                {this.state.answered && (
                    <Typography className="description" variant="h5" color="error">
                        {`Correct answers: ${this.state.correctAnswers} / ${quiz.questions.length}`}
                    </Typography>
                )}
                {this.props.answers && (
                    <Typography className="description" variant="h5" color="error">
                        {this.props.answers}
                    </Typography>
                )}
                <TextDialog
                    handleClose={this.handleClose}
                    open={this.state.open}
                    text={JSON.stringify(this.state.feedback, null, 2)}
                />
            </React.Fragment>
        );
    }
}

Quiz.propTypes = {
    quiz: PropTypes.shape({
        id: PropTypes.string,
        code: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        active: PropTypes.bool,
        questions: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number,
            text: PropTypes.string,
            type: PropTypes.string,
            answers: PropTypes.arrayOf(PropTypes.shape({
                id: PropTypes.number,
                text: PropTypes.string,
            })),
        })),
    }).isRequired,
    answers: PropTypes.string.isRequired,
    resetAnswers: PropTypes.func.isRequired,
    setAnswer: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    quiz: state.quiz.quiz,
    answers: JSON.stringify(state.answers.answers),
    results: state.results,
});

const mapDispatchToProps = dispatch => ({
    resetAnswers: () => dispatch(resetAnswersAction()),
    setAnswer: (id, answer) => dispatch(setAnswerAction(id, answer)),
    submitResults: (correctAnswers, mistakes) => dispatch(
        submitResults(correctAnswers, mistakes),
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(Quiz);
