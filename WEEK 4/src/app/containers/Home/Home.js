import React from 'react';
import PropTypes from 'prop-types';
import connect from 'react-redux/es/connect/connect';

import { Header, QuizCodeForm } from 'components';
import { setQuiz } from 'state-management/actions/quiz';
import { resetResults } from 'state-management/actions/results';
import quiz from 'data/quiz';
import Button from 'components/Button/Button';
import SesionResult from '../SessionResult/SessionResult';

const defaultQuiz = {
    id: 'some-other-id',
    code: 'default',
    title: 'Default quiz when code not right',
    description: 'hamlet',
    active: true,
    questions: [
        {
            id: 2,
            text: 'Why?',
            type: 'LONG',
            correctAnswers: ['yes', 'no', 'maybe'],
        },
    ],
};

class Home extends React.Component {
    render() {
        console.log(this.props.results)
        return (
            <React.Fragment>
                <Header title="Feedback Matters" />
                <QuizCodeForm
                    isLoading={this.props.isLoading}
                    error={this.props.error}
                    submitForm={this.props.getQuiz}
                    activeCode={this.props.quizCode}
                />
                {this.props.results.map(session => (
                    <SesionResult sessionId={session.quizId} key={session.quizId} />
                ))}
                <SesionResult sessionId="total" />
                <button onClick={() => {
                    this.props.resetResults();
                }}
                >
                    {' '}
                    RESET
                </button>
            </React.Fragment>

        );
    }
}

Home.propTypes = {
    isLoading: PropTypes.bool,
    error: PropTypes.string,
    getQuiz: PropTypes.func,
    quizCode: PropTypes.string,
};

Home.defaultProps = {
    isLoading: false,
    error: '',
    getQuiz: () => {
    },
    quizCode: '',
};

const mapStateToProps = state => ({
    isLoading: state.quiz.isLoading,
    error: state.quiz.error,
    quizCode: state.quiz.quiz.code,
    results: state.results.answers,
});

const mapDispatchToProps = dispatch => ({

    resetResults: () => dispatch(resetResults()),
    getQuiz: (code) => {
        const newQuiz = quiz.code === code ? quiz : defaultQuiz;
        dispatch(setQuiz(newQuiz));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
