import React from 'react';
import connect from 'react-redux/es/connect/connect';
import SessionResultComponent from 'components/SessionResult/SessionResultComponent';

class SessionResult extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            quizId: this.props.results.quizId,
        });
    }

    getSessionResults=(sessionId) => {
        if (sessionId === 'total') {
            return this.totalScores();
        }
        return this.props.results.find(session => session.quizId === sessionId);
    }

    totalScores = () => {
        let totalTries = 0;
        let correctAnswers = 0;
        let mistakes = 0;
        this.props.results.forEach((session) => {
            totalTries += session.scores.tries,
            mistakes += session.scores.mistakes,
            correctAnswers += session.scores.correctAnswers;
        });
        return {
            quizId: 'Total',
            scores: {
                correctAnswers,
                mistakes,
                tries: totalTries,
            },
        };
    }

    render() {
        const quizId = this.props.match == null ? this.props.sessionId : this.props.match.params.quizid;
        const results = this.getSessionResults(quizId);
        return (
            <SessionResultComponent quizId={this.props.sessionId} results={results} />
        );
    }
}
const mapStateToProps = state => ({
    results: state.results.answers,
});

export default connect(mapStateToProps, null)(SessionResult);
