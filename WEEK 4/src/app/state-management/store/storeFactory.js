import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import createBrowserHistory from 'history/createBrowserHistory';

import reducers from 'state-management/reducers';

function storeFactory() {
    const middleware = applyMiddleware(
        routerMiddleware(createBrowserHistory()),
    );

    /* eslint-disable no-underscore-dangle */
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    /* eslint-enable */

    return createStore(reducers, composeEnhancers(middleware));
}

export default storeFactory();
