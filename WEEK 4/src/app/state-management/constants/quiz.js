export const GET_QUIZ = 'quiz/GET_QUIZ';
export const GET_QUIZ_SUCCESS = 'quiz/GET_QUIZ_SUCCESS';
export const GET_QUIZ_ERROR = 'quiz/GET_QUIZ_ERROR';
export const SET_QUIZ = 'quiz/SET_QUIZ';
