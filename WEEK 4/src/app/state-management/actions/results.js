import {
    RESET_RESULTS, SUBMIT_RESULTS,
} from 'state-management/constants/session';


export const resetResults = results => ({
    type: RESET_RESULTS,
    results,
});
export const submitResults = results => ({
    type: SUBMIT_RESULTS,
    results,
});
