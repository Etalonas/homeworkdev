import {
    RESET_ANSWERS,
    SET_ANSWER,
} from 'state-management/constants/answers';

export const resetAnswers = () => ({
    type: RESET_ANSWERS,
});

export const setAnswer = (questionId, answer) => ({
    type: SET_ANSWER,
    questionId,
    answer,
});
