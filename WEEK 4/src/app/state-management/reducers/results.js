import {
    RESET_RESULTS, SUBMIT_RESULTS,
} from 'state-management/constants/session';


const initialResults = {
    answers: [
        {
            quizId: 'VLN_4',
            scores: {
                correctAnswers: 8,
                mistakes: 0,
                tries: 3,
            },
        },
        {
            quizId: 'initial',
            scores: {
                correctAnswers: 1,
                mistakes: 15,
                tries: 10,
            },
        },

    ],
};
const copy = {
    answers: [
        {
            quizId: 'VLN_4',
            scores: {
                correctAnswers: 8,
                mistakes: 0,
                tries: 3,
            },
        },
        {
            quizId: 'initial',
            scores: {
                correctAnswers: 1,
                mistakes: 15,
                tries: 10,
            },
        },

    ],
};
export default function quizReducer(state = initialResults, action = {}) {
    switch (action.type) {
    case RESET_RESULTS: {
        return copy;
    }
    case SUBMIT_RESULTS: {
        const session = state.answers.find(quiz => (quiz.quizId === action.results.quizId));
        session.scores.correctAnswers += action.results.correctAnswers;
        session.scores.tries += 1;
        session.scores.mistakes += action.results.totalQuestions - action.results.correctAnswers;
    }
    default:
        return state;
    }
}
