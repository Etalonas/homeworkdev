import {
    RESET_ANSWERS,
    SET_ANSWER,
} from 'state-management/constants/answers';

const initialState = {
    answers: {
        // 1: '4',
        // 2: 'i dont know ',
        // 3: 'katinas',
        // 4: ['Option X', 'Option 2'],
    },
};

export default function answerReducer(state = initialState, action = {}) {
    switch (action.type) {
    case RESET_ANSWERS: return initialState;
    case SET_ANSWER: return {
        ...state,
        answers: {
            ...state.answers,
            [action.questionId]: action.answer,
        },
    };
    default:
        return state;
    }
}
