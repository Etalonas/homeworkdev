import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import results from 'state-management/reducers/results';
import quiz from 'state-management/reducers/quiz';
import answers from 'state-management/reducers/answers';

export default combineReducers({
    router: routerReducer,
    quiz,
    answers,
    results,
});
