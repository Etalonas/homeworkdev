import React from 'react';
import { shallow } from 'enzyme';
import QuizCodeForm from 'components/QuizCodeForm/QuizCodeForm';

describe('Session result button', () => {
    it('should be disabled', () => {
        const sessionButton = shallow(<QuizCodeForm activeCode="initial" />).find('.session-button');
        expect(sessionButton.props().disabled).toBe(true);
    });
});
