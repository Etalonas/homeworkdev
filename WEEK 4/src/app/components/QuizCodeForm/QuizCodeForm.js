import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField/TextField';
import Button from '@material-ui/core/Button/Button';
import Typography from '@material-ui/core/Typography/Typography';
import { Link } from 'react-router-dom';
import connect from 'react-redux/es/connect/connect';

import './quizCodeForm.scss';
import { GET_QUIZ } from 'state-management/constants/quiz';

class QuizCodeForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            code: '',
        };
    }

    handleChange = (e) => {
        const code = e.target.value.trim();

        if (code !== this.state.code) {
            this.setState({
                code,
            });
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.submitForm(this.state.code);
    };

    isDisabled() {
        const { isLoading } = this.props;
        const { code } = this.state;

        return code.length <= 3 || isLoading;
    }

    render() {
        const { error, activeCode } = this.props;
        return (
            <form className="quiz-code-form" noValidate onSubmit={this.handleSubmit}>
                <div className="quiz-code">
                    <TextField
                        error={!!error}
                        label={error || 'Quiz Code'}
                        margin="normal"
                        className="quiz-text-field"
                        onChange={this.handleChange}
                        required
                    />
                </div>
                <Button
                    disabled={this.isDisabled()}
                    variant="contained"
                    color="primary"
                    size="large"
                    type="submit"
                >
                    Set quiz
                </Button>
                <Link to="/generator">
                    <Button
                        variant="contained"
                        color="secondary"
                        size="large"
                    >
                        New
                    </Button>
                </Link>
                <div>
                    <Typography className="description" variant="h5" color="inherit">
                        {`Active quiz: ${activeCode}`}
                    </Typography>
                    <Link to="/quiz">
                        <Button
                            variant="contained"
                            color="primary"
                            size="large"
                        >
                            Go to quiz
                        </Button>
                    </Link>
                    <Link to={`/session/${activeCode}`}>
                        <Button
                            className="session-button"
                            variant="contained"
                            color="secondary"
                            size="large"
                            disabled={activeCode === 'initial'}
                        >
                          Go to session results
                        </Button>
                    </Link>
                </div>
            </form>
        );
    }
}

QuizCodeForm.defaultProps = {
    activeCode: '',
};

QuizCodeForm.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    submitForm: PropTypes.func.isRequired,
    error: PropTypes.string.isRequired,
    activeCode: PropTypes.string,
};

const mapStateToProps = state => ({
    results: state.results,

});

export default connect(mapStateToProps, null)(QuizCodeForm);
