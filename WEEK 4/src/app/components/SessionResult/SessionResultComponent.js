import React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button/Button';
import { Link } from 'react-router-dom';


class SessionResultComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            quizId: this.props.quizId,
        });
    }

    render() {
        const results = this.props.results;
        const { quizId } = this.state;
        return (
            <div>
                <Paper elevation={1}>
                    <Typography variant="h5" component="h3">
                        Quiz with quizID:
                        {' '}
                        {quizId}
                    </Typography>
                    <Typography component="p">
                        Total tries :
                        {' '}
                        {results.scores.tries}
                        {' '}
                        , correctAnswers:
                        {' '}
                        {results.scores.correctAnswers}
                        , mistakes :
                        {' '}
                        {results.scores.mistakes}
                    </Typography>
                </Paper>
            </div>
        );
    }
}

export default SessionResultComponent;
