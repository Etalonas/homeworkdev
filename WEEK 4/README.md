# Feedback Frontend

Lecture 4 practise
npm install history react-redux react-router-dom react-router-redux redux --save

Lecture 4 homework

Create page for test-quiz results for current session
#1 create blank placeholder page for session results
#2 add navigation link to said page
#3 create reducer for session results (with some dummy results in initial state)
#4 add results container to placeholder page
#5 connect it to redux store
#6 map session results redux store state to props
#7 display session results
#8 add action creators to track session results
#9 dispatch actions from quiz container (when form is submitted)
#10 happy dance

